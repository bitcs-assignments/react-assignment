import { useEffect, useState } from "react";
import $ from "jquery";

function TaskCard(props) {
  const og = props.task.name;
  const [value, setValue] = useState(props.task.name);
  const [toggle, setToggle] = useState(true);
  const [cross, setCross] = useState(false);

  useEffect(() => setValue(props.task.name));

  const getStrike = () => {
    if (props.task.isDone) return <strike> {value} </strike>;
    return <span> {value} </span>;
  };

  const handleChange = (e) => {
    setValue(e.target.value);
    let newTask = [...props.allTasks];
    newTask[props.index].name = e.target.value;
    props.updateTask(newTask);
  };

  const handleBlur = () => {
    if (value === "") props.deleteTask(props.index);
    setToggle(true);
  }

  const handleCheckClick = () => {
    props.toggleComplete(props.index);
    if ($(`#${props.index}`).css("color") === "rgb(255, 255, 255)") {
      $(`#${props.index}`).css('color', 'green');
    } else {
      $(`#${props.index}`).css('color', 'white');
    }
  }

  const handleKeyDown = (e) => {
    if (e.key === "Enter") handleBlur();
    if (e.key === "Escape") {
      setValue(og);
      handleBlur();
    }
  }

  return (
    <div className="TaskCard">
      <div
        className="list lefty"
        onMouseEnter={() => setCross(true)}
        onMouseLeave={() => setCross(false)}
      >
        {/* <button
        type="button"
        className="btn btn-outline-success lefty"
        onClick={() => props.completeTask(props.index)}
      >
        ✓
      </button> */}
        {/* <input
          type="checkbox"
          name="tick"
          defaultChecked={isAllCompleted()}
          id={props.index}
          onClick={() => props.toggleComplete(props.index)}
        /> */}
        <div className="checkers" id={props.index} onClick={handleCheckClick}>
          &#10004;
        </div>
        {toggle ? (
          <span onDoubleClick={() => setToggle(false)}> {getStrike()} </span>
        ) : (
          <input
            type="text"
            value={value}
            onKeyDown={handleKeyDown}
            onBlur={handleBlur}
            onChange={handleChange}
          />
        )}
        <button
          type="button"
          className="cross"
          onClick={() => props.deleteTask(props.index)}
        >
          {cross && <span>&#x2715;</span>}
        </button>
      </div>
    </div>
  );
}

export default TaskCard;

// ❌
